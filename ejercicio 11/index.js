const names = [
  'A-Jay',
  'Manuel',
  'Manuel',
  'Eddie',
  'A-Jay',
  'Su',
  'Reean',
  'Manuel',
  'A-Jay',
  'Zacharie',
  'Zacharie',
  'Tyra',
  'Rishi',
  'Arun',
  'Kenton',
];

console.log(names);

function Repeated(array) {
  const newArray = [];

  for (const item of array) {
    if (newArray.includes(item) === false) {
      newArray.push(item);
    }
  }

  return newArray;
}

console.log(Repeated(names));
