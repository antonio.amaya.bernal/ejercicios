const URL = 'https://rickandmortyapi.com/api/episode';

async function getCharacterName(url) {
  try {
    const episodes = [];
    const response = await fetch(`${url}`);
    const data = await response.json();

    const pages = data.info.pages;

    for (let index = 1; index <= pages; index++) {
      if (data.info.next != null) {
        const response = await fetch(`${url}?page=${index}`);
        const data = await response.json();

        const episodes = data.results.filter((episode) => {
          if (episode.air_date.includes('January')) {
            episodes.push(episode.characters);
          }
        });
      }
    }

    const charNames = [];

    for (const character of episodes) {
      for (let index = 0; index < character.length; index++) {
        const response = await fetch(character[index]);
        const data = await response.json();

        charNames.push(data.name);
      }
    }

    console.log(charNames);
  } catch (error) {
    console.log(error);
  }
}

getCharacterName(URL);
